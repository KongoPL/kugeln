/**
 * @module js-logger
 * @description Typescript description for js-logger
 */

declare interface ILogLevel extends Object {
  /**
   * The numerical representation of the level
   */
  value: number;
  /**
   * Human readable name of the log level
   */
  name: string;
}

declare interface IContext extends Object {
  /**
   * The currrent log level
   */
  level: ILogLevel;
  /**
   * The optional current logger name
   */
  name?: string;
}

/**
 * Defines custom formatter for the log message
 * @callback formatterCallback
 * @param  {any[]}    messages the given logger arguments
 * @param  {IContext} context  the current logger context (level and name)
 */

declare interface ILoggerOpts extends Object {
  /**
   * The log level, default is DEBUG
   */
  defaultLevel?: ILogLevel;

  /**
   * Defines custom formatter for the log message
   * @param  {formatterCallback} callback the callback which handles the formatting
   */
  formatter?: (messages: any[], context: IContext) => void;
}

/**
 * Defines custom handler for the log message
 * @callback setHandlerCallback
 * @param  {any[]}    messages the given logger arguments
 * @param  {IContext} context  the current logger context (level and name)
 */

declare class Logger {
  static DEBUG: ILogLevel;
  static INFO: ILogLevel;
  static TIME: ILogLevel;
  static WARN: ILogLevel;
  static ERROR: ILogLevel;
  static OFF: ILogLevel;

  static debug(...x: any[]): void;
  public static info(...x: any[]): void;
  public static log(...x: any[]): void;
  static warn(...x: any[]): void;
  static error(...x: any[]): void;

  /**
   * Configure and example a Default implementation which writes to the
   * `window.console` (if present). The `options` hash can be used to configure
   * the default logLevel and provide a custom message formatter.
   */
  static useDefaults(options?: ILoggerOpts): void;

  /**
   * Sets the global logging filter level which applies to *all* previously
   * registered, and future Logger instances. (note that named loggers (retrieved
   * via `Logger.get`) can be configured independently if required).
   *
   * @param  {ILogLevel} level the level to switch to
   */
  static setLevel(level: ILogLevel): void;
  /**
   * Gets the global logging filter level
   *
   * @return {ILogLevel} the current logging level
   */
  static getLevel(): ILogLevel;
   /**
   * Set the global logging handler. The supplied function should
   * expect two arguments, the first being an arguments object with the
   * supplied log messages and the second being a context object which
   * contains a hash of stateful parameters which the logging function can consume.
   * @param  {setHandlerCallback} callback the callback which handles the logging
   */
  static setHandler(logHandler: (messages: any[], context: IContext) => void): void;
  /**
   * Retrieve a ContextualLogger instance.  Note that named loggers automatically
   * inherit the global logger's level, default context and log handler.
   *
   * @param  {string}  name the logger name
   * @return {ILogger}      the named logger
   */
  static get(name: string): Logger;
  static time(label: string): void;
  static timeEnd(label: string): void;
  static enabledFor(level: ILogLevel): boolean;
  static createDefaultHandler(options?: ILoggerOpts): (messages: any[], context: IContext) => void;
}

//declare var Logger: CLogger;
//export = Logger;

/*declare module "Logger" {
	export = Logger;
}*/
