"use strict";
var ServerHandler = (function () {
    function ServerHandler(client) {
        this.client = client;
        for (var c in this) {
            if (Type.isFunction(this[c]) && c.substr(0, 2) === "On") {
                client.on(c.substr(2), this[c]);
            }
        }
    }
    ServerHandler.prototype.OnGameSync = function (data) {
        for (var id in data) {
            PlayerManager.SyncPlayer(id, data[id].position, data[id].movementDirection);
        }
    };
    ServerHandler.prototype.OnPlayerDirectionChange = function (data) {
        PlayerManager.SyncPlayerMovementDirection(data.id, { x: data.x, y: data.y });
    };
    ServerHandler.prototype.OnPing = function (time) {
        var ping = Math.round((new Date().getTime() - time) / 2);
        document.querySelector("#ping").innerHTML = ping + " ms";
    };
    ServerHandler.prototype.OnRequestServerInfo = function (data) {
        Logger.log(data);
        document.querySelector("#tickrate").innerHTML = data.tickRate.toString();
        document.querySelector("#synctick").innerHTML = data.syncTick + " ticks (" + (data.syncTick / data.tickRate * 1000) + " ms)";
    };
    return ServerHandler;
}());
