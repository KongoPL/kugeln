"use strict";
var PlayerManager = (function () {
    function PlayerManager() {
    }
    PlayerManager.SpawnCount = function (count) {
        count -= this.players.length;
        for (var i = 0; i < count; i++) {
            var randomX = MathHelper.RandFloat(0.1, 0.9) * Canvas.worldBounds.width, randomY = MathHelper.RandFloat(0.1, 0.9) * Canvas.worldBounds.height;
            var player = new Player(randomX, randomY, Canvas.worldGroup);
            this.players.push(player) - 1;
            if (this.spectatorMode) {
                var spectatorPlayer = new Player(randomX, randomY, Canvas.spectatorGroup);
                spectatorPlayer.id = player.id;
                this.players.push(spectatorPlayer) - 1;
            }
            ServerManagement.SendMessage("Join", { id: player.id, x: randomX, y: randomY });
        }
    };
    PlayerManager.SyncPlayer = function (id, position, movementDirection) {
        for (var c in this.players) {
            var player = this.players[c];
            if (player.id == id) {
                player.SetPosition(position);
                player.SetMovementDirection(movementDirection, true);
            }
        }
    };
    PlayerManager.SyncPlayerMovementDirection = function (id, movementDirection) {
        for (var c in this.players) {
            var player = this.players[c];
            if (player.id == id) {
                player.SetMovementDirection(movementDirection, true);
            }
        }
    };
    PlayerManager.GenerateUniqueId = function (length) {
        if (length === void 0) { length = 12; }
        var chars = "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
        var id = "";
        for (var i = 0; i < length; i++) {
            id += chars[MathHelper.Rand(0, chars.length - 1)];
        }
        return id;
    };
    PlayerManager.players = [];
    PlayerManager.spectatorMode = true;
    return PlayerManager;
}());
