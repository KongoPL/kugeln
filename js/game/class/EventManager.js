"use strict";
var EventManager = (function () {
    function EventManager() {
    }
    EventManager.AddListener = function (eventName, callback, context) {
        if (context === void 0) { context = this; }
        if (Type.isUndefined(this.listeners[eventName])) {
            this.listeners[eventName] = [];
        }
        if (this.GetListener(eventName, callback) == null) {
            var object = {
                callback: callback,
                context: context
            };
            this.listeners[eventName].push(object);
            return true;
        }
        else {
            return false;
        }
    };
    EventManager.RemoveListener = function (eventName, callback) {
        var listeners = this.GetListeners(eventName);
        for (var c in listeners) {
            if (listeners[c].callback == callback) {
                this.listeners[eventName].splice(c, 1);
                return true;
            }
        }
        return false;
    };
    EventManager.Emit = function (eventName) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        var listeners = this.GetListeners(eventName);
        for (var c in listeners) {
            listeners[c].callback.apply(listeners[c].context, args);
        }
    };
    EventManager.GetRegistredEventNames = function () {
        var names = [];
        for (var c in this.listeners) {
            names.push(c);
        }
        return names;
    };
    EventManager.GetListener = function (eventName, callback) {
        var listeners = this.GetListeners(eventName);
        for (var c in listeners) {
            if (listeners[c].callback == callback) {
                return listeners[c];
            }
        }
        return null;
    };
    EventManager.GetListeners = function (eventName) {
        return Type.isUndefined(this.listeners[eventName]) ? [] : this.listeners[eventName];
    };
    EventManager.listeners = {};
    return EventManager;
}());
