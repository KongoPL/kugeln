"use strict";
var ColiderDetection = (function () {
    function ColiderDetection() {
    }
    ColiderDetection.WorldBounds = function (object, worldBounds) {
        if (worldBounds === void 0) { worldBounds = Canvas.worldBounds; }
        var objectBounds = object.getBounds();
        if (objectBounds.left <= worldBounds.left)
            return ECollidedWall.LEFT;
        else if (objectBounds.right >= worldBounds.right)
            return ECollidedWall.RIGHT;
        else if (objectBounds.top <= worldBounds.top)
            return ECollidedWall.TOP;
        else if (objectBounds.bottom >= worldBounds.bottom)
            return ECollidedWall.BOTTOM;
        else
            return false;
    };
    return ColiderDetection;
}());
var ECollidedWall;
(function (ECollidedWall) {
    ECollidedWall[ECollidedWall["LEFT"] = 0] = "LEFT";
    ECollidedWall[ECollidedWall["RIGHT"] = 1] = "RIGHT";
    ECollidedWall[ECollidedWall["TOP"] = 2] = "TOP";
    ECollidedWall[ECollidedWall["BOTTOM"] = 3] = "BOTTOM";
})(ECollidedWall || (ECollidedWall = {}));
