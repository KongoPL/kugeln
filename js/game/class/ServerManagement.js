"use strict";
var ServerManagement = (function () {
    function ServerManagement() {
    }
    ServerManagement.Connect = function (onConnected) {
        if (onConnected === void 0) { onConnected = function () { }; }
        if (typeof io == 'undefined') {
            var _this_1 = this;
            this.LoadSocketFile(function () {
                _this_1.Connect(onConnected);
            });
        }
        else {
            this.connection = io('http://' + Config.getOption("serverAddr"));
            new ServerHandler(this.connection);
            onConnected();
        }
    };
    ServerManagement.SendMessage = function (messageName, data) {
        this.connection.emit(messageName, data);
    };
    ServerManagement.LoadSocketFile = function (callback) {
        loadFile('http://' + Config.getOption("serverAddr") + '/socket.io/socket.io.js', callback);
    };
    return ServerManagement;
}());
