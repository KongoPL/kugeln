"use strict";
var Canvas = (function () {
    function Canvas() {
    }
    Canvas.Init = function () {
        this.game = new Phaser.Game(this.width, this.height, Phaser.AUTO, 'game', {
            preload: this.OnPreload,
            create: this.OnCreate,
            update: this.OnUpdate
        });
    };
    Canvas.CreateImage = function (x, y, src, group) {
        return this.game.add.image(x, y, src, 0, group);
    };
    Canvas.CreateSprite = function (x, y, src, frame, group) {
        return this.game.add.sprite(x, y, src, frame, group);
    };
    Canvas.OverlayGroups = function () {
        this.areGroupsOverlayed = !this.areGroupsOverlayed;
        if (this.areGroupsOverlayed) {
            this.spectatorGroup.position.setTo(this.worldBounds.x, this.worldBounds.y);
            this.spectatorGroup.setAllChildren("alpha", 0.5);
            this.worldGroup.setAllChildren("alpha", 0.5);
        }
        else {
            this.spectatorGroup.position.setTo(this.spectatorBounds.x, this.spectatorBounds.y);
            this.spectatorGroup.setAllChildren("alpha", 1);
            this.worldGroup.setAllChildren("alpha", 1);
        }
    };
    Canvas.OnPreload = function () {
        var filesToLoad = [
            "images/bg.jpg",
            "images/object.png",
            "images/split-space.png"
        ];
        for (var c in filesToLoad) {
            this.game.load.image(filesToLoad[c], filesToLoad[c]);
        }
        this.game.world.setBounds(0, 0, this.width, this.height);
    };
    Canvas.OnCreate = function () {
        Canvas.worldGroup = this.game.add.group();
        Canvas.spectatorGroup = this.game.add.group();
        Canvas.worldGroup.position.setTo(Canvas.worldBounds.x, Canvas.worldBounds.y);
        Canvas.worldGroup.width = Canvas.worldBounds.width;
        Canvas.worldGroup.height = Canvas.worldBounds.height;
        Canvas.spectatorGroup.position.setTo(Canvas.spectatorBounds.x, Canvas.spectatorBounds.y);
        Canvas.spectatorGroup.width = Canvas.spectatorBounds.width;
        Canvas.spectatorGroup.height = Canvas.spectatorBounds.height;
        EventManager.Emit("Canvas.Created");
        Canvas.game.world.bringToTop(Canvas.worldGroup);
        Canvas.game.world.bringToTop(Canvas.spectatorGroup);
    };
    Canvas.OnUpdate = function () {
        EventManager.Emit("Canvas.Update", this.game.time.elapsed / 1000);
    };
    Canvas.worldBounds = new PIXI.Rectangle(0, 0, 800, 600);
    Canvas.spectatorBounds = new PIXI.Rectangle(850, 0, 800, 600);
    Canvas.width = 1650;
    Canvas.height = 600;
    Canvas.areGroupsOverlayed = false;
    return Canvas;
}());
