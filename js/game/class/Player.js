"use strict";
var Player = (function () {
    function Player(x, y, group) {
        if (x === void 0) { x = 500; }
        if (y === void 0) { y = 500; }
        if (group === void 0) { group = Canvas.worldGroup; }
        this.id = "";
        this.movementDirection = new Phaser.Point(0, 0);
        this.changeMovementDirectionWithin = 0;
        this.movementSpeed = 0;
        this.id = PlayerManager.GenerateUniqueId();
        this.movementSpeed = Config.getOption("player.movementSpeed");
        this.sprite = Canvas.CreateSprite(x, y, "images/object.png");
        this.sprite.anchor.setTo(0.5, 0.5);
        group.add(this.sprite);
        this.EnableMovement();
        this.spawnedInWorld = (group == Canvas.worldGroup);
    }
    Player.prototype.EnableMovement = function () {
        var _this = this;
        EventManager.AddListener("Canvas.Update", function (deltaTime) {
            _this.Update(deltaTime);
        });
    };
    Player.prototype.Update = function (deltaTime) {
        this.changeMovementDirectionWithin -= deltaTime;
        var colidesWorldbounds = ColiderDetection.WorldBounds(this.sprite, (this.spawnedInWorld ? Canvas.worldBounds : Canvas.spectatorBounds));
        if (this.spawnedInWorld && (colidesWorldbounds !== false || this.changeMovementDirectionWithin <= 0)) {
            this.ChangeMovementDirection(colidesWorldbounds);
            this.changeMovementDirectionWithin = Config.getOption("player.movementChangeTime");
        }
        if (colidesWorldbounds) {
            this.sprite.x = this.sprite.previousPosition.x - this.sprite.parent.x;
            this.sprite.y = this.sprite.previousPosition.y - this.sprite.parent.y;
        }
        else {
            this.sprite.x += this.movementDirection.x * this.movementSpeed * deltaTime;
            this.sprite.y += this.movementDirection.y * this.movementSpeed * deltaTime;
        }
    };
    Player.prototype.ChangeMovementDirection = function (wall) {
        if (wall === ECollidedWall.LEFT || wall === ECollidedWall.RIGHT) {
            this.SetMovementDirection({
                x: -this.movementDirection.x,
                y: this.movementDirection.y
            });
        }
        else if (wall === ECollidedWall.TOP || wall === ECollidedWall.BOTTOM)
            this.SetMovementDirection({
                x: this.movementDirection.x,
                y: -this.movementDirection.y
            });
        else {
            var angle = MathHelper.Rand(0, 360);
            this.SetMovementDirection({
                x: Math.sin(angle / 180 * Math.PI),
                y: Math.cos(angle / 180 * Math.PI)
            });
        }
    };
    Player.prototype.SetPosition = function (position) {
        this.sprite.x = position.x;
        this.sprite.y = position.y;
    };
    Player.prototype.SetMovementDirection = function (movementDirection, givenFromListener) {
        if (givenFromListener === void 0) { givenFromListener = false; }
        this.movementDirection.setTo(movementDirection.x, movementDirection.y);
        if (givenFromListener == false) {
            ServerManagement.SendMessage("MovementDirectionChange", {
                id: this.id,
                x: this.movementDirection.x,
                y: this.movementDirection.y
            });
        }
    };
    return Player;
}());
