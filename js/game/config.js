"use strict";
var Config = (function () {
    function Config() {
    }
    Config.getOption = function (option) {
        var options = option.split(".");
        var returningValue = this.options;
        for (var c in options) {
            var key = options[c];
            if (Type.isUndefined(returningValue[key]) == false) {
                returningValue = returningValue[key];
            }
            else {
                return;
            }
        }
        if (Type.isObject(returningValue) == false) {
            return returningValue;
        }
    };
    Config.setOption = function (option, value) {
        var options = option.split(".");
        var returningValue = this.options;
        for (var c in options) {
            var key = options[c];
            if (Type.isUndefined(returningValue[key]) == false) {
                if (parseInt(c) == options.length - 1) {
                    returningValue[key] = value;
                    return true;
                }
                else {
                    returningValue = returningValue[key];
                }
            }
            else {
                return false;
            }
        }
    };
    Config.getOptions = function () {
        return JSON.parse(JSON.stringify(this.options));
    };
    Config.options = {
        "version": "1.0",
        "serverAddr": "127.0.0.1:1234",
        "player": {
            "movementSpeed": 50,
            "movementChangeTime": 5
        }
    };
    return Config;
}());
