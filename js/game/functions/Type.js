"use strict";
var Type = (function () {
    function Type() {
    }
    Type.isUndefined = function (variable) {
        return (typeof variable == 'undefined');
    };
    Type.isObject = function (variable) {
        return (typeof variable == 'object');
    };
    Type.isFunction = function (variable) {
        return (typeof variable == 'function');
    };
    return Type;
}());
