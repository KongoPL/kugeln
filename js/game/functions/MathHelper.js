"use strict";
var MathHelper = (function () {
    function MathHelper() {
    }
    MathHelper.Rand = function (min, max) {
        return Math.round(this.RandFloat(min, max));
    };
    MathHelper.RandFloat = function (min, max) {
        return Math.random() * (max - min) + min;
    };
    return MathHelper;
}());
