var filesToLoad = [],
	isLoadingFiles = false;

function include( filePath )
{
	filesToLoad.push( filePath );

	if ( isLoadingFiles == false )
	{
		loadFirstFile();
	}
}

function loadFirstFile()
{
	isLoadingFiles = true;

	loadFile( "js/" + filesToLoad[0], function ()
	{
		loadNextFileIfPossible();
	}, function ()
		{
			console.error( "Unable to load " + scriptPath );

			loadNextFileIfPossible();
		} );
	
}

function loadNextFileIfPossible()
{
	filesToLoad.shift();

	if ( filesToLoad.length > 0 )
	{
		loadFirstFile();
	}
	else
	{
		isLoadingFiles = false;
	}
}


function loadFile( scriptPath, onSuccess, onError )
{
	var script = document.createElement( "script" );

	script.onload = onSuccess
	script.onerror = onError;
	script.src = scriptPath + "?" + new Date().getTime();

	document.getElementsByTagName( "head" )[0].appendChild( script );
}