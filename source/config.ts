class Config
{
	protected static options: any = {
		"version": "1.0",
		"serverAddr": "127.0.0.1:1234",
		"player": {
			"movementSpeed": 50,
			"movementChangeTime": 5
		}
	};


	public static getOption( option: string ): any
	{
		let options = option.split( "." );
		let returningValue = this.options;

		for ( let c in options )
		{
			let key = options[c];

			if ( Type.isUndefined( returningValue[key] ) == false )
			{
				returningValue = returningValue[key];
			}
			else
			{
				return;
			}
		}

		if ( Type.isObject( returningValue ) == false )
		{
			return returningValue;
		}
	}

	public static setOption( option: string, value: any ): any
	{
		let options = option.split( "." );
		let returningValue = this.options;

		for ( let c in options )
		{
			let key = options[c];

			if ( Type.isUndefined( returningValue[key] ) == false )
			{
				if ( parseInt( c ) == options.length - 1 )
				{
					returningValue[key] = value;

					return true;
				}
				else
				{
					returningValue = returningValue[key];
				}
			}
			else
			{
				return false;
			}
		}
	}


	public static getOptions()
	{
		return JSON.parse( JSON.stringify( this.options ) );
	}
}