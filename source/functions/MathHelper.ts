class MathHelper
{
	public static Rand( min: number, max: number ): number
	{
		return Math.round( this.RandFloat( min, max ) );
	}


	public static RandFloat( min: number, max: number ): number
	{
		return Math.random() * ( max - min ) + min;
	}
}