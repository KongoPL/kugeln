class Type
{
	public static isUndefined( variable: any ): boolean
	{
		return ( typeof variable == 'undefined' );
	}

	public static isObject( variable: any ): boolean
	{
		return ( typeof variable == 'object' );
	}

	public static isFunction( variable: any ): boolean
	{
		return ( typeof variable == 'function' );
	}
}