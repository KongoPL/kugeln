/// <reference path="../declarations/phaser.d.ts" />
/// <reference path="../declarations/logger.d.ts" />

Logger.useDefaults();
Canvas.Init();


EventManager.AddListener( "Canvas.Created", function ()
{
	Canvas.CreateImage( 0, 0, "images/bg.jpg" );
	Canvas.CreateImage( Canvas.worldBounds.width, 0, "images/split-space.png" );

	ServerManagement.Connect( function ()
	{
		PlayerManager.SpawnCount( 25 );

		ServerManagement.SendMessage( "RequestServerInfo" );

		setInterval( function ()
		{
			ServerManagement.SendMessage( "Ping", new Date().getTime() );
		}, 1000 );
	});
} );