class EventManager
{
	// Array of IListener where key is event name.
	protected static listeners: any = {};

	public static AddListener( eventName: string, callback: (...args: any[]) => void, context: object = this ): boolean
	{
		if ( Type.isUndefined( this.listeners[eventName] ) )
		{
			this.listeners[eventName] = [];
		}

		if ( this.GetListener( eventName, callback ) == null )
		{
			let object: IListener = {
				callback: callback,
				context: context
			}

			this.listeners[eventName].push( object );

			return true;
		}
		else
		{
			return false;
		}
	}

	public static RemoveListener( eventName: string, callback: ( ...args: any[] ) => void ): boolean
	{
		let listeners = this.GetListeners( eventName );

		for ( let c in listeners )
		{
			if ( listeners[c].callback == callback )
			{
				this.listeners[eventName].splice( c, 1 );

				return true;
			}
		}

		return false;
	}


	public static Emit( eventName: string, ...args: any[] ): void
	{
		let listeners = this.GetListeners( eventName );

		for ( let c in listeners )
		{
			listeners[c].callback.apply( listeners[c].context, args );
		}
	}


	/**
	 * Used mainly for debugging and checking.
	 */
	public static GetRegistredEventNames(): string[]
	{
		var names: string[] = [];

		for ( let c in this.listeners )
		{
			names.push( c );
		}

		return names;
	}


	protected static GetListener( eventName: string, callback: ( ...args: any[] ) => void ): IListener | null
	{
		let listeners = this.GetListeners( eventName );

		for ( let c in listeners )
		{
			if ( listeners[c].callback == callback )
			{
				return listeners[c];
			}
		}

		return null;
	}


	protected static GetListeners( eventName: string ): IListener[]
	{
		return Type.isUndefined( this.listeners[eventName] ) ? [] : this.listeners[eventName];
	}
}

interface IListener
{
	callback: ( ...args: any[] ) => void,
	context: object
}