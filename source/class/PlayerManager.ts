class PlayerManager
{
	public static players: Player[] = [];
	public static spectatorMode: boolean = true;

	public static SpawnCount( count: number ): void
	{
		count -= this.players.length;

		for ( var i = 0; i < count; i++ )
		{
			let spawnX: number = MathHelper.RandFloat( 0.1, 0.9 ) * Canvas.worldBounds.width,
				spawnY: number = MathHelper.RandFloat( 0.1, 0.9 ) * Canvas.worldBounds.height;

			let player = new Player( spawnX, spawnY, Canvas.worldGroup );

			this.players.push( player ) - 1;

			if ( this.spectatorMode )
			{
				let spectatorPlayer = new Player( spawnX, spawnY, Canvas.spectatorGroup );

				spectatorPlayer.id = player.id;

				this.players.push( spectatorPlayer ) - 1;
			}

			ServerManagement.SendMessage( "Join", {
				id: player.id,
				x: spawnX,
				y: spawnY
			} );
		}
	}


	public static SyncPlayer( id: string, position: ICoords, movementDirection: ICoords )
	{
		for ( let c in this.players )
		{
			let player = this.players[c];

			if ( player.id == id )
			{
				player.SetPosition( position );
				player.SetMovementDirection( movementDirection, true );
			}
		}
	}


	public static SyncPlayerMovementDirection( id: string, movementDirection: ICoords )
	{
		for ( let c in this.players )
		{
			let player = this.players[c];

			if ( player.id == id )
			{
				player.SetMovementDirection( movementDirection, true );
			}
		}
	}
}


interface ICoords
{
	x: number,
	y: number
}