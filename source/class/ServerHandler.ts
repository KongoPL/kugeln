class ServerHandler
{
	protected client: ISocketIOClient;

	constructor( client: ISocketIOClient )
	{
		this.client = client;

		for ( var c in this )
		{
			if ( Type.isFunction( this[c] ) && c.substr( 0, 2 ) === "On" )
			{
				client.on( c.substr( 2 ), this[c] );
			}
		}
	}


	public OnGameSync( data: IOnGameSync )
	{
		for ( let id in data )
		{
			PlayerManager.SyncPlayer( id, data[id].position, data[id].movementDirection );
		}
	}


	public OnPlayerDirectionChange( data: IOnPlayerDirectionChange )
	{
		PlayerManager.SyncPlayerMovementDirection( data.id, { x: data.x, y: data.y } );
	}


	public OnPing( time: number )
	{
		let ping = Math.round( ( new Date().getTime() - time ) / 2 );

		document.querySelector( "#ping" ).innerHTML = ping + " ms";
	}


	public OnRequestServerInfo( data: IOnRequestServerInfo )
	{
		document.querySelector( "#tickrate" ).innerHTML = data.tickRate.toString();
		document.querySelector( "#synctick" ).innerHTML = data.syncTick + " ticks (" + ( data.syncTick / data.tickRate * 1000 ) + " ms)";
	}
}

interface IOnGameSync
{
	[keys: string]: {
		position: {
			x: number,
			y: number
		},
		movementDirection: {
			x: number,
			y: number
		}
	}
}

interface IOnPlayerDirectionChange
{
	id: string,
	x: number,
	y: number
}

interface IOnRequestServerInfo
{
	tickRate: number,
	syncTick: number
}

interface ISocketIOClient
{
	/**
	 * Unique identifies for the session
	*/
	id: string;

	/**
	 * Sends message to client.
	 *
	 * eventName: Event name
	 * data: Sending data (any kind)
	 * [ack]: What to do when client answers on your message
	*/
	emit( eventName: string, data: any, ack?: ( data?: any ) => void ): void;

	/**
	 * Adds listener for `eventName
	*/
	on( eventName: string, callback: any ): void;
}