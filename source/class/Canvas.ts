class Canvas
{
	public static game: Phaser.Game;

	public static worldBounds = new PIXI.Rectangle( 0, 0, 800, 600 );
	public static worldGroup: Phaser.Group;

	public static spectatorBounds = new PIXI.Rectangle( 850, 0, 800, 600 );
	public static spectatorGroup: Phaser.Group;

	protected static areGroupsOverlayed: boolean = false;

	public static width: number = 1650;
	public static height: number = 600;

	public static Init()
	{
		this.game = new Phaser.Game( this.width, this.height, Phaser.AUTO, 'game', {
			preload: this.OnPreload,
			create: this.OnCreate,
			update: this.OnUpdate
		} );
	}

	public static CreateImage( x: number, y: number, src: string, group?: Phaser.Group ): Phaser.Image
	{
		return this.game.add.image( x, y, src, 0, group );
	}

	public static CreateSprite( x: number, y: number, src: string, frame?: number, group?: Phaser.Group ): Phaser.Sprite
	{
		return this.game.add.sprite( x, y, src, frame, group );
	}

	public static OverlayGroups(): void
	{
		this.areGroupsOverlayed = !this.areGroupsOverlayed;

		if ( this.areGroupsOverlayed )
		{
			this.spectatorGroup.position.setTo( this.worldBounds.x, this.worldBounds.y );
			this.spectatorGroup.setAllChildren( "alpha", 0.5 );
			this.worldGroup.setAllChildren( "alpha", 0.5 );
		}
		else
		{
			this.spectatorGroup.position.setTo( this.spectatorBounds.x, this.spectatorBounds.y );
			this.spectatorGroup.setAllChildren( "alpha", 1 );
			this.worldGroup.setAllChildren( "alpha", 1 );
		}
	}


	protected static OnPreload()
	{
		let filesToLoad = [
			"images/bg.jpg",
			"images/object.png",
			"images/split-space.png"
		];

		for ( let c in filesToLoad )
		{
			this.game.load.image( filesToLoad[c], filesToLoad[c] );
		}

		this.game.world.setBounds( 0, 0, this.width, this.height );
	}

	protected static OnCreate()
	{
		//Create canvas viewing groups: player's view and spectator's view:
		Canvas.worldGroup = this.PrepareGameGroup( this.game.add.group(), Canvas.worldBounds );
		Canvas.spectatorGroup = this.PrepareGameGroup( this.game.add.group(), Canvas.spectatorBounds );

		EventManager.Emit( "Canvas.Created" );

		Canvas.game.world.bringToTop( Canvas.worldGroup );
		Canvas.game.world.bringToTop( Canvas.spectatorGroup );
	}


	private static PrepareGameGroup( group: Phaser.Group, bounds: PIXI.Rectangle ): Phaser.Group
	{
		group.position.setTo( bounds.x, bounds.y );
		group.width = bounds.width;
		group.height = bounds.height;

		return group;
	}


	protected static OnUpdate()
	{
		EventManager.Emit( "Canvas.Update", this.game.time.elapsed / 1000 );
	}
}