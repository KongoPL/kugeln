declare var io: any;
declare function loadFile( path: string, onSuccess?: () => void, onError?: () => void ): void;

class ServerManagement
{
	protected static connection: any;

	public static Connect( onConnected: () => void = function () { } ): void
	{
		if ( typeof  io == 'undefined' )
		{
			let _this = this;

			this.LoadSocketFile( function ()
			{
				_this.Connect( onConnected );
			});
		}
		else
		{
			this.connection = io( 'http://' + Config.getOption( "serverAddr" ) );

			new ServerHandler( this.connection );

			onConnected();
		}
	}


	public static SendMessage( messageName: string, data?: any )
	{
		this.connection.emit( messageName, data );
	}


	protected static LoadSocketFile( callback: () => void )
	{
		loadFile( 'http://' + Config.getOption( "serverAddr" ) + '/socket.io/socket.io.js', callback );
	}
}