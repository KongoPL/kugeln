class Player
{
	public id: string = "";

	protected sprite: Phaser.Sprite;
	protected movementDirection = new Phaser.Point( 0, 0 );
	protected changeMovementDirectionWithin: number = 0;
	protected movementSpeed: number = 0;

	protected isSpawnedInWorld: boolean;

	constructor( x: number = 500, y: number = 500, group: Phaser.Group = Canvas.worldGroup )
	{
		this.id = this.GenerateUniqueId();
		this.movementSpeed = Config.getOption( "player.movementSpeed" );
		this.isSpawnedInWorld = ( group == Canvas.worldGroup );

		this.sprite = Canvas.CreateSprite( x, y, "images/object.png" );
		this.sprite.anchor.setTo( 0.5, 0.5 );

		group.add( this.sprite );

		this.EnableMovement();
	}


	public EnableMovement(): void
	{
		let _this = this;

		EventManager.AddListener( "Canvas.Update", function ( deltaTime: number )
		{
			_this.Update( deltaTime );
		} );
	}


	/**
	 * Updates player.
	 * @param deltaTime	Time elapsed from last frame (in seconds)
	 */
	public Update( deltaTime: number ): void
	{
		this.changeMovementDirectionWithin -= deltaTime;

		let colidesWorldbounds: any = ColiderDetection.WorldBounds( this.sprite, ( this.isSpawnedInWorld ? Canvas.worldBounds : Canvas.spectatorBounds ) );

		if ( this.isSpawnedInWorld && ( colidesWorldbounds !== false || this.changeMovementDirectionWithin <= 0 ) )
		{
			this.ChangeMovementDirection( colidesWorldbounds );
			this.changeMovementDirectionWithin = Config.getOption( "player.movementChangeTime" );
		}

		if ( colidesWorldbounds )
		{
			this.sprite.x = this.sprite.previousPosition.x - this.sprite.parent.x;
			this.sprite.y = this.sprite.previousPosition.y - this.sprite.parent.y;
		}
		else
		{
			this.sprite.x += this.movementDirection.x * this.movementSpeed * deltaTime;
			this.sprite.y += this.movementDirection.y * this.movementSpeed * deltaTime;
		}
	}


	protected ChangeMovementDirection( wall?: ECollidedWall ): void
	{
		// Reaction on hitting any wall:
		if ( wall === ECollidedWall.LEFT || wall === ECollidedWall.RIGHT )
		{
			this.SetMovementDirection( {
				x: -this.movementDirection.x,
				y: this.movementDirection.y
			} );
		}
		else if ( wall === ECollidedWall.TOP || wall === ECollidedWall.BOTTOM )
			this.SetMovementDirection( {
				x: this.movementDirection.x,
				y: -this.movementDirection.y
			} );
		else
		{
			// Just change on random angle

			let angle: number = MathHelper.Rand( 0, 360 );

			this.SetMovementDirection( {
				x: Math.sin( angle / 180 * Math.PI ),
				y: Math.cos( angle / 180 * Math.PI )
			} );
		}
	}


	public SetPosition( position: ICoords )
	{
		this.sprite.x = position.x;
		this.sprite.y = position.y;
	}


	public SetMovementDirection( movementDirection: ICoords, givenFromListener: boolean = false )
	{
		this.movementDirection.setTo( movementDirection.x, movementDirection.y );

		if ( givenFromListener == false )
		{
			ServerManagement.SendMessage( "MovementDirectionChange", {
				id: this.id,
				x: this.movementDirection.x,
				y: this.movementDirection.y
			} );
		}
	}


	protected GenerateUniqueId( length: number = 12 ): string
	{
		let chars: string = "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
		let id: string = "";

		for ( let i = 0; i < length; i++ )
		{
			id += chars[MathHelper.Rand( 0, chars.length - 1 )];
		}

		return id;
	}
}