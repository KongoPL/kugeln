class ColiderDetection
{
	/**
	 * Checks whether objects colides with world bounds.
	 * @param object
	 */
	public static WorldBounds( object: Phaser.Image, worldBounds: PIXI.Rectangle = Canvas.worldBounds ): ECollidedWall | boolean
	{
		let objectBounds: PIXI.Rectangle = object.getBounds();

		if ( objectBounds.left <= worldBounds.left ) return ECollidedWall.LEFT;
		else if ( objectBounds.right >= worldBounds.right ) return ECollidedWall.RIGHT;
		else if ( objectBounds.top <= worldBounds.top ) return ECollidedWall.TOP;
		else if ( objectBounds.bottom >= worldBounds.bottom ) return ECollidedWall.BOTTOM;
		else return false;
	}
}

enum ECollidedWall
{
	LEFT,
	RIGHT,
	TOP,
	BOTTOM
}